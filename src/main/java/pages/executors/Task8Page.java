package pages.executors;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import pages.locators.Task8PageLocators;

import java.util.concurrent.TimeUnit;
public class Task8Page {
    private static WebDriver driver;
    private Task8PageLocators locators;

    public Task8Page(WebDriver driver) {
        this.driver = driver;
        locators = new Task8PageLocators();
        PageFactory.initElements(driver, locators);
    }

    public Task8Page selectCardType(String type) {
        Select select = new Select(locators.cardTypeSelect);
        select.selectByVisibleText(type);
        return this;
    }

    public Task8Page supplyName(String name) {
        locators.nameInput.sendKeys(name);
        return this;
    }

    public Task8Page cardNumberInput(String cardNumber) {
        locators.cardNumberInput.sendKeys(cardNumber);
        return this;
    }

    public Task8Page cardCvvInput (String cvvNumber) {
        locators.cardCvvInput.sendKeys(cvvNumber);
        return this;
    }

    public Task8Page monthSelect(String month) {
        Select selectMonth = new Select(locators.monthSelect);
        selectMonth.selectByVisibleText(month);
        return this;
    }

    public Task8Page yearSelect(String year) {
        Select selectYear = new Select(locators.yearSelect);
        selectYear.selectByVisibleText(year);
        return this;
    }

    public Task8Page payButton() {
        locators.payButton.click();
        return this;
    }

    public String getMessage() {
        return locators.successMessage.getText();
    }


}
