package pages.locators;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class Task8PageLocators {

    @FindBy(how = How.ID, using = "task8_form_cardType")
    public WebElement cardTypeSelect;

    @FindBy(how = How.ID, using = "task8_form_name")
    public WebElement nameInput;

    @FindBy(how = How.ID, using = "task8_form_cardNumber")
    public WebElement cardNumberInput;

    @FindBy(how = How.ID, using = "task8_form_cardCvv")
    public WebElement cardCvvInput;

    @FindBy(how = How.ID, using = "task8_form_cardInfo_month")
    public WebElement monthSelect;

    @FindBy(how = How.ID, using = "task8_form_cardInfo_year")
    public WebElement yearSelect;

    @FindBy(how = How.NAME, using = "task8_form[save]")
    public WebElement payButton;

    @FindBy(how = How.XPATH, using = "/html/body/div/div/div[2]/div/div")
    public WebElement successMessage;
}
