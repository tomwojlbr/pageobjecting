package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.edge.EdgeDriver;

import java.util.concurrent.TimeUnit;


public class SeleniumDriver {

    public final static int TIMEOUT = 600;
    public final static int PAGE_LOAD_TIMEOUT = 600;

    private static SeleniumDriver seleniumDriver;
    private static WebDriver driver;

    private SeleniumDriver() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\drivers\\ChromeDriver" +
                ".exe");
        driver = new ChromeDriver();

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
    }

    public static void openPage(String url) {
        driver.navigate().to(url);
    }

    public static WebDriver getDriver() {
        return driver;
    }

    public static void setUpDriver() {
        if (seleniumDriver == null)
            seleniumDriver = new SeleniumDriver();
    }

    public static void tearDown() {
        if (driver != null) {
            driver.close();
            driver.quit();
        }
        seleniumDriver = null;
    }
}