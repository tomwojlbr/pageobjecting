package utils;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

public class SeleniumHelper {

    public static boolean isElementPresent(By by) {
        try {
            SeleniumDriver.getDriver().findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }
}