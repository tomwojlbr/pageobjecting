import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.executors.Task8Page;

import java.util.concurrent.TimeUnit;


public class Page8Test {
    private static WebDriver driver;

    @BeforeClass
    public static void setUp() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\drivers\\ChromeDriver" +
                ".exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().pageLoadTimeout(600, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Test
    public void testTask8() {
        driver.navigate().to("https://testingcup.pgs-soft.com/task_8");

        Task8Page task8Page = new Task8Page(driver);

        task8Page.selectCardType("American Express");
        task8Page.supplyName("Pierre Dolinsky");
        task8Page.cardNumberInput("371449635398431");
        task8Page.cardCvvInput("666");
        task8Page.monthSelect("February");
        task8Page.yearSelect("2021");
        task8Page.payButton();
        Assert.assertEquals("Zamówienie opłacone", task8Page.getMessage());

    }

    @AfterClass
    public static void tearDown() {
        driver.close();
        driver.quit();

    }
}
