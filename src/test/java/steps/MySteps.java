package steps;

//import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import pages.executors.Task8Page;
import utils.SeleniumDriver;


public class MySteps {

    Task8Page task8Page = new Task8Page(SeleniumDriver.getDriver());

    @Given("^I am on page for task (\\d+)$")
    public void iAmOnPageForTask(int arg0) throws Throwable {
        SeleniumDriver.openPage("https://testingcup.pgs-soft.com/task_8");
    }

    @When("^I select card type$")
    public void iSelectCardType() throws Throwable {
        task8Page.selectCardType("American Express");
    }

    @And("^supply my name$")
    public void supplyMyName() throws Throwable {
        task8Page.supplyName("Pierre Dolinsky");
    }

    @And("^I give my credit card details$")
    public void iGiveMyCreditCardDetails() throws Throwable {
        task8Page.cardNumberInput("371449635398431");
    }

    @And("^I give the cvv code$")
    public void iGiveTheCvvCode() throws Throwable {
        task8Page.cardCvvInput("666");
    }

    @And("^I select month$")
    public void iSelectMonth() throws Throwable {
        task8Page.monthSelect("February");
    }

    @And("^I select year$")
    public void iSelectYear() throws Throwable {
        task8Page.yearSelect("2021");
    }

    @And("^click the zaplac button$")
    public void clickTheZaplacButton() throws Throwable {
        task8Page.payButton();
    }

    @Then("^I see message \"([^\"]*)\"$")
    public void iSeeMessage(String arg0) throws Throwable {
        Assert.assertEquals("Zamówienie opłacone", task8Page.getMessage());
    }
}
